type Timeslot = {
  start_time: number;
  end_time: number;
};
type AvailableRecord = Timeslot;
type BookingRecord = Timeslot;

function toTime(time: string) {
  let [hour, minute] = time.split(':');
  return parseInt(hour) * 60 + parseInt(minute);
}
function fromTime(time: number) {
  let hour = Math.floor(time / 60);
  let minute = time - hour * 60;
  let text = hour + ':' + minute;
  if (minute < 10) {
    text += '0';
  }
  return text;
}

// from database
let availableTimes: AvailableRecord[] = [
  { start_time: toTime('10:00'), end_time: toTime('13:00') },
  { start_time: toTime('14:00'), end_time: toTime('19:00') },
];
// from database
let bookedTimes: AvailableRecord[] = [
  { start_time: toTime('15:00'), end_time: toTime('16:00') },
];
// from user request (browser)
let requestTime: Timeslot = {
  start_time: toTime('14:00'),
  end_time: toTime('17:00'),
};

function prepareTimeslots() {
  let timeslots: boolean[] = [];

  // prepare empty timeslots
  for (let i = toTime('00:00'); i < toTime('23:59'); i++) {
    timeslots[i] = false;
  }

  // mark available timeslots
  for (let availableTime of availableTimes) {
    for (let i = availableTime.start_time; i < availableTime.end_time; i++) {
      timeslots[i] = true;
    }
  }

  // mark already booked timeslots
  for (let bookedTime of bookedTimes) {
    for (let i = bookedTime.start_time; i < bookedTime.end_time; i++) {
      timeslots[i] = false;
    }
  }

  return timeslots;
}

function canBook() {
  let timeslots = prepareTimeslots();

  // check if any time clash
  for (let i = requestTime.start_time; i < requestTime.end_time; i++) {
    if (!timeslots[i]) {
      return false;
    }
  }
  return true;
}

console.log('can book?', canBook());

function findRemindingAvailableTimtslots(): Timeslot[] {
  let timeslots = prepareTimeslots();
  let remindingAvailableTimtslots: Timeslot[] = [];
  let mode = 'find start time';
  let start_time;
  let end_time;
  for (let i = 0; i < timeslots.length; i++) {
    if (mode == 'find start time' && timeslots[i]) {
      start_time = i;
      mode = 'find end time';
    } else if (mode == 'find end time' && !timeslots[i]) {
      end_time = i;
      remindingAvailableTimtslots.push({ start_time, end_time });
      mode = 'find start time';
    }
  }
  if (mode == 'find end time') {
    end_time = timeslots.length;
    remindingAvailableTimtslots.push({ start_time, end_time });
  }
  return remindingAvailableTimtslots;
}
function report() {
  let remindingAvailableTimtslots = findRemindingAvailableTimtslots();
  console.log('remindingAvailableTimtslots:');
  for (let remindingAvailableTimtslot of remindingAvailableTimtslots) {
    let startTime = fromTime(remindingAvailableTimtslot.start_time);
    let endTime = fromTime(remindingAvailableTimtslot.end_time);
    console.log('from', startTime, 'to', endTime);
  }
}
report();
